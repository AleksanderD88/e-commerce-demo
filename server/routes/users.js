const router = require("express").Router();
const { createUser, loginUser } = require("../controllers/UserController");

exports.auth = router
  .post("/api/create", createUser)
  .post("/api/login", loginUser);
