const User = require("../model/UserModel");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");

exports.createUser = async (req, res) => {
  const credentials = req.body;
  if (Object.values(credentials).some((input) => input === ""))
    return res.json(400).json({
      error: "Couldn't proceed with the request, try again at a later time.",
    });
  const userExist = await User.findOne({ email: credentials.email });
  if (userExist)
    return res.json(400).json({
      error: "Couldn't proceed with the request, try again at a later time.",
    });

  const passwordMatch = credentials.password === credentials.confirmPassword;
  if (!passwordMatch)
    return res.status(400).json({ error: "Password didn't match" });

  const user = new User({
    fullName: `${credentials.firstName} ${credentials.lastName}`,
    email: credentials.email,
    password: await bcrypt.hash(credentials.password, 10),
  });

  const payload = {
    userID: user._id,
    fullName: user.fullName,
    email: user.email,
  };

  const token = jwt.sign(payload, process.env.SECRET_KEY, {
    expiresIn: 60 * 60,
  });

  const response = {
    name: user.fullName,
    email: user.email,
  };

  user
    .save()
    .then(() => {
      res
        .cookie("usr_auth", token, {
          httpOnly: true,
          maxAge: 60 * 60 * 1000,
          secure: process.env.NODE_ENV === "production",
        })
        .status(201)
        .json(response);
    })
    .catch((err) => res.status(500).json(err));
};
exports.loginUser = async (req, res) => {
  const credentials = req.body;
  if (Object.values(credentials).some((input) => input === ""))
    return res.status(500).json({
      error: "Couldn't proceed with the request, try again at a later time.",
    });

  const user = await User.findOne({ email: credentials.email });
  if (!user)
    return res.status(500).json({
      error: "Couldn't proceed with the request, try again at a later time.",
    });

  const matchedPassword = await bcrypt.compare(
    credentials.password,
    user.password
  );
  if (!matchedPassword)
    return res.status(500).json({
      error: "Couldn't proceed with the request, try again at a later time.",
    });

  const payload = {
    userID: user._id,
    fullName: user.fullName,
    email: user.email,
  };

  const token = jwt.sign(payload, process.env.SECRET_KEY, {
    expiresIn: 60 * 60,
  });

  try {
    res
      .cookie("usr_auth", token, {
        httpOnly: true,
        maxAge: 60 * 60 * 1000,
        secure: process.env.NODE_ENV === "production",
      })
      .status(201)
      .json({ message: "Success" });
  } catch (error) {
    res.status(500).json(error);
  }
};
