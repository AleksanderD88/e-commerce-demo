const mongoose = require("../db");

const schema = new mongoose.Schema({
  fullName: {
    desc: "The users first name and last name",
    trim: true,
    type: String,
    required: true,
  },
  email: {
    desc: "The users email address",
    trim: true,
    type: String,
    index: true,
    unique: true,
    required: true,
  },
  password: {
    desc: "Users password",
    trim: true,
    type: String,
    required: true,
  },
});

module.exports = mongoose.model("Users", schema);
