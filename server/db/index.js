const mongoose = require("mongoose");

mongoose.connect(process.env.DB_URL, (err) => {
  if (err) return console.log(err);
  console.log("Successfully connected to database =)");
});

module.exports = mongoose;
