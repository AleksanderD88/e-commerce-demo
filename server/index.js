require("dotenv/config");
const express = require("express");
const app = express();
const cors = require("cors");
const cookieParser = require("cookie-parser");
const PORT = process.env.PORT || 5000;
const { auth } = require("./routes/users");

app.use(cors());
app.use(cookieParser());
app.use(express.json());

app.use("/", auth);

// Test response
app.get("/api/started", (req, res) =>
  res.send("Server is up and running, happy coding =)")
);

app.listen(PORT, () => console.log(`Server running on port: ${PORT}`));
