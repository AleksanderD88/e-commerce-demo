module.exports = {
  devServer: {
    proxy: {
      "^/api": {
        target: "http://server:5000",
      },
    },
  },
  css: {
    loaderOptions: {
      sass: {
        prependData: `
          @import "@/styles/variables/__variables-dir.scss";
          @import "@/styles/mixins/__mixins-dir.scss";
        `,
      },
    },
  },
};
