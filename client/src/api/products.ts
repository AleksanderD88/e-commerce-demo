import client from "./httpClient";

import store from "@/store";
import { AxiosResponse } from "axios";

client.interceptors.request.use((req) => {
  store.commit("spinner/TOGGLE_SPINNER");
  return req;
});

client.interceptors.response.use((res) => {
  store.commit("spinner/TOGGLE_SPINNER");
  return res;
});

export const getOneProduct = (id: string | number): Promise<AxiosResponse> => {
  return client.get("https://fakestoreapi.com/products/" + id);
};

export const getAllProducts = (): Promise<AxiosResponse> => {
  return client.get("https://fakestoreapi.com/products");
};
