import client from "./httpClient";

import { AxiosResponse } from "axios";

export const createUser = (credentials: any): Promise<AxiosResponse> => {
  return client.post("/api/create", credentials);
};

export const loginUser = (credentials: any): Promise<AxiosResponse> => {
  return client.post("/api/login", credentials);
};
