import { Commit } from "vuex";
import { ISidebarState } from "./types";

export default {
  namespaced: true,
  state: (): ISidebarState => ({
    sidebarOpen: false,
  }),
  mutations: {
    TOGGLE_SIDEBAR(state: ISidebarState): void {
      state.sidebarOpen = !state.sidebarOpen;
    },
  },
  actions: {
    toggleSidebar({ commit }: { commit: Commit }): void {
      commit("TOGGLE_SIDEBAR");
    },
  },
};
