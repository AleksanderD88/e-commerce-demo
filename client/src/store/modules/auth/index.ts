import { createUser, loginUser } from "@/api/auth";
import { Commit } from "vuex";

export default {
  namespaced: true,
  state: () => ({}),
  mutations: {},
  actions: {
    createUser({ commit }: { commit: Commit }, credentials: any) {
      return createUser(credentials);
    },
    loginUser({ commit }: { commit: Commit }, credentials: any) {
      return loginUser(credentials);
    },
  },
};
