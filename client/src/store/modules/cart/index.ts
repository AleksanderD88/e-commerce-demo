import { Commit } from "vuex";
import { IProduct } from "../product/types";

export interface ICartState {
  cartOpen: boolean;
  cartItems: IProduct[];
  count: number;
}

export default {
  namespaced: true,
  state: (): ICartState => ({
    cartOpen: false,
    cartItems: [],
    count: 1,
  }),
  mutations: {
    TOGGLE_CART(state: ICartState): void {
      state.cartOpen = !state.cartOpen;
    },
    ADD_PRODUCT(state: ICartState, product: IProduct): void {
      const newProduct = Object.assign({ count: state.count }, product);
      const itemExist = state.cartItems.find((item) => item.id === product.id);
      if (itemExist) {
        itemExist.count++;
        return;
      }
      state.cartItems.push(newProduct);
    },
    SET_RECENT_CART(state: ICartState, products: IProduct[]): void {
      state.cartItems = products;
    },
  },
  actions: {
    addToCart({ commit }: { commit: Commit }, product: IProduct): void {
      commit("ADD_PRODUCT", product);
    },
  },
  getters: {
    checkCartOpen(state: ICartState): boolean {
      return !!state.cartOpen;
    },
    displayCartItems(state: ICartState): number {
      return state.cartItems.reduce((sum, item) => {
        return (sum += Number(item.count));
      }, 0);
    },
    displayCartTotal(state: ICartState): number {
      return state.cartItems.reduce((sum, item) => {
        return (sum += item.count * item.price);
      }, 0);
    },
    filterCartItems: (state: ICartState) => (
      product: IProduct
    ): Array<IProduct> => {
      return (state.cartItems = state.cartItems.filter(
        (item) => item.id !== product.id
      ));
    },
  },
};
