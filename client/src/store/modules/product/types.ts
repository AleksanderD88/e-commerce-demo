export interface IState {
  product: Record<string, unknown>;
}

export interface IProduct {
  id: string;
  category: string;
  description: string;
  image: string;
  price: number;
  title: string;
  count: number;
}
