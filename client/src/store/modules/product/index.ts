import { getOneProduct } from "@/api/products";
import { Commit } from "vuex";
import { IProduct, IState } from "./types";

export default {
  namespaced: true,
  state: (): IState => ({
    product: {},
  }),
  mutations: {
    SET_PRODUCT(state: IState, product: { product: IProduct }): void {
      state.product = product;
    },
  },
  actions: {
    getOneProduct({ commit }: { commit: Commit }, id = 3): Promise<IProduct> {
      return getOneProduct(id).then(({ data }: { data: IProduct }) => {
        commit("SET_PRODUCT", data);
        return data;
      });
    },
  },
  getters: {},
};
