export interface ISpinnerState {
  enabled: boolean;
}

export default {
  namespaced: true,
  state: (): ISpinnerState => ({
    enabled: false,
  }),
  mutations: {
    TOGGLE_SPINNER(state: ISpinnerState): void {
      state.enabled = !state.enabled;
    },
  },
};
