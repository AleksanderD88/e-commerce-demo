import { getAllProducts } from "@/api/products";
import { Commit } from "vuex";
import { IProduct } from "../product/types";

export interface IProductsState {
  products: IProduct[];
}

export default {
  namespaced: true,
  state: (): IProductsState => ({
    products: [],
  }),
  mutations: {
    SET_PRODUCTS(state: IProductsState, data: IProduct[]): void {
      state.products = data;
    },
  },
  actions: {
    getAllProducts({ commit }: { commit: Commit }): Promise<IProduct> {
      return getAllProducts().then(
        ({ data }): Promise<IProduct> => {
          commit("SET_PRODUCTS", data);
          return data;
        }
      );
    },
  },
  getters: {},
};
