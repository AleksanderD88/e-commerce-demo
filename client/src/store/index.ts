import { createStore } from "vuex";
import product from "./modules/product";
import products from "./modules/products";
import cart from "./modules/cart";
import spinner from "./modules/spinner";
import sidebar from "./modules/sidebar";
import auth from "./modules/auth";

export default createStore({
  modules: {
    product,
    products,
    cart,
    spinner,
    sidebar,
    auth,
  },
});
