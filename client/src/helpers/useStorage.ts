import { IProduct } from "@/store/modules/product/types";
import { encryptStorage } from "@/utils";

export const getDataStorage = (key: string) => {
  const storedData = encryptStorage.getItem(key);
  if (storedData) return storedData;
};

export const setDataStorage = (key: string, value: IProduct[]) =>
  encryptStorage.setItem(key, JSON.stringify(value));
