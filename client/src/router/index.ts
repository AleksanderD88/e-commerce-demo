import { createRouter, createWebHistory, RouteRecordRaw } from "vue-router";
import Home from "../views/Home.vue";
import store from "@/store";

const routes: Array<RouteRecordRaw> = [
  {
    path: "/",
    name: "Home",
    component: Home,
    beforeEnter(to, from, next) {
      store.dispatch("product/getOneProduct").then(() => {
        next();
      });
    },
  },
  {
    path: "/products",
    name: "Products",
    props: true,
    component: () => import("@/views/Products.vue"),
    beforeEnter(to, from, next) {
      store.dispatch("products/getAllProducts").then((data) => {
        to.params.products = data;
        next();
      });
    },
  },
  {
    path: "/p/:id/:title",
    name: "Product",
    props: true,
    component: () => import("@/views/Product.vue"),
    beforeEnter(to, from, next) {
      store.dispatch("product/getOneProduct", to.params.id).then((data) => {
        to.params.product = data;
        next();
      });
    },
  },
  {
    path: "/terms",
    name: "Terms",
    component: () => import("@/views/Terms.vue"),
  },
  {
    path: "/user",
    name: "User",
    component: () => import("@/views/User/User.vue"),
    children: [
      {
        path: "signup",
        name: "Signup",
        component: () => import("@/views/User/Form/Signup.vue"),
      },
      {
        path: "login",
        name: "Login",
        component: () => import("@/views/User/Form/Login.vue"),
      },
      {
        path: "favorites",
        name: "Favorites",
        component: () => import("@/views/User/Favorites.vue"),
      },
    ],
  },
];

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
  scrollBehavior: () => {
    return { left: 0, top: 0 };
  },
});

export default router;
