# E Commerce Demo

A demo site demonstrating an actual e-commerce site with payment options

## Getting started

Start the app using docker ( need docker installed on your system ). Visit this site for more info: [https://www.docker.com/get-started].

## Getting started (without docker installed)

After cloning, cd into respective directory and run **npm i** to install all dependencies. In **client** start up development server by typing in terminal **npm run serve** and from **server** type in terminal **npm run dev**. To check if server is started, go to **http://localhost:5000/api/started**. You should be greeted with a response with the following text: **Server is up and running, happy coding =)**
